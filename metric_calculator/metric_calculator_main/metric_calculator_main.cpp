#include "parameters.h"
#include "cyclomatic_complexity.h"
#include "function_lines.h"
#include "comments_density.h"
#include "depth.h"
#include "line_sender.h"
#include <iostream>

using namespace std;

int main(int argc, char** argv)
{
    parameters fileOpener(argc, argv);

    cyclomatic_complexity cyclomatic_metric;
    function_lines function_metric;
    comments_density comments_metric;
    depth depth_metric;

    line_sender sender;

    sender.add_metric(&cyclomatic_metric);
    sender.add_metric(&function_metric);
    sender.add_metric(&comments_metric);
    sender.add_metric(&depth_metric);
    sender.extract_lines(fileOpener.input_stream());


    cout << cyclomatic_metric.metric_result() << ","
         << function_metric.metric_result() << ","
         << comments_metric.metric_result() << ","
         << depth_metric.metric_result() << endl;

    return sender.calculate_star_rating();

}
