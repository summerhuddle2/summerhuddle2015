#include "parameters.h"
#include "gtest/gtest.h"

using namespace testing;

TEST(parameters, A_file_that_exists_is_indicated_as_existing)
{
    char parameter0[] = "dummy";
    char parameter1[] = "example.txt";
    char* argv[]={parameter0, parameter1};
    parameters file_exists (2,argv);

    ASSERT_TRUE(file_exists.exists());
}

TEST(parameters, A_file_that_does_not_exist_is_indicated_as_not_existing)
{
    char parameter0[] = "dummy";
    char parameter1[] = "example2.txt";
    char* argv[]={parameter0, parameter1};
    parameters file_exists (2,argv);

    ASSERT_FALSE(file_exists.exists());
}
