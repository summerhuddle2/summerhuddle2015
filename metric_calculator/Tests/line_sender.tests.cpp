#include "line_sender.h"
#include <gtest/gtest.h>

#include <string>
#include <sstream>
#include <vector>

using namespace ::testing;

class metric_stub : public metric
{
public:
    metric_stub() : result(1) {}
    std::vector<std::string> lines;
    virtual void parse_line(const std::string & line)
    {
        lines.push_back(line);
    }
    int result;
    virtual int metric_result ()
    {
        return result;
    }
};

TEST(line_sender, an_input_stream_with_single_line_is_sent_to_a_single_metric)
{
    std::string line = "the answer is 42";
    std::stringstream input_stream(line);
    metric_stub metric;

    line_sender sender;
    sender.add_metric(&metric);
    sender.extract_lines(input_stream);

    ASSERT_EQ(static_cast<size_t>(1), metric.lines.size());
    ASSERT_EQ("the answer is 42", metric.lines[0]);

}


TEST(line_sender, an_input_stream_with_two_lines_is_sent_to_a_single_metric)
{
    std::string line = "the answer is 42\nthe answer is 43";
    std::stringstream input_stream(line);
    metric_stub metric;

    line_sender sender;
    sender.add_metric(&metric);
    sender.extract_lines(input_stream);

    ASSERT_EQ(static_cast<size_t>(2), metric.lines.size());
    ASSERT_EQ("the answer is 42", metric.lines[0]);
    ASSERT_EQ("the answer is 43", metric.lines[1]);

}

TEST(line_sender, an_input_stream_with_two_lines_is_sent_to_multiple_metrics)
{
    std::string line = "the answer is 42\nthe answer is 43";
    std::stringstream input_stream(line);
    metric_stub metric1;
    metric_stub metric2;
    metric_stub metric3;

    line_sender sender;
    sender.add_metric(&metric1);
    sender.add_metric(&metric2);
    sender.add_metric(&metric3);


    sender.extract_lines(input_stream);

    ASSERT_EQ(static_cast<size_t>(2), metric1.lines.size());
    ASSERT_EQ("the answer is 42", metric1.lines[0]);
    ASSERT_EQ("the answer is 43", metric1.lines[1]);

    ASSERT_EQ(static_cast<size_t>(2), metric2.lines.size());
    ASSERT_EQ("the answer is 42", metric2.lines[0]);
    ASSERT_EQ("the answer is 43", metric2.lines[1]);

    ASSERT_EQ(static_cast<size_t>(2), metric3.lines.size());
    ASSERT_EQ("the answer is 42", metric3.lines[0]);
    ASSERT_EQ("the answer is 43", metric3.lines[1]);

}

TEST(line_sender, 3_passing_metrics_return_5_star_rating)
{
    metric_stub metric1;
    metric_stub metric2;
    metric_stub metric3;
    metric1.result = 1;
    metric2.result = 1;
    metric3.result = 1;

    line_sender sender;

    sender.add_metric(&metric1);
    sender.add_metric(&metric2);
    sender.add_metric(&metric3);

    ASSERT_EQ( 5, sender.calculate_star_rating() );

}

TEST(line_sender, 3_failing_metrics_return_0_star_rating)
{
    metric_stub metric1;
    metric_stub metric2;
    metric_stub metric3;
    metric1.result = 0;
    metric2.result = 0;
    metric3.result = 0;

    line_sender sender;

    sender.add_metric(&metric1);
    sender.add_metric(&metric2);
    sender.add_metric(&metric3);

    ASSERT_EQ( 0, sender.calculate_star_rating() );

}

TEST(line_sender, 2_failing_metrics_and_3_passing_metrics_return_3_star_rating)
{
    metric_stub metric1;
    metric_stub metric2;
    metric_stub metric3;
    metric_stub metric4;
    metric_stub metric5;

    metric1.result = 1;
    metric2.result = 0;
    metric3.result = 1;
    metric4.result = 0;
    metric5.result = 1;

    line_sender sender;

    sender.add_metric(&metric1);
    sender.add_metric(&metric2);
    sender.add_metric(&metric3);
    sender.add_metric(&metric4);
    sender.add_metric(&metric5);

    ASSERT_EQ( 3, sender.calculate_star_rating() );
}

TEST(line_sender, 3_failing_metrics_and_3_passing_metrics_return_3_star_rating)
{
    metric_stub metric1;
    metric_stub metric2;
    metric_stub metric3;
    metric_stub metric4;
    metric_stub metric5;
    metric_stub metric6;

    metric1.result = 1;
    metric2.result = 0;
    metric3.result = 1;
    metric4.result = 0;
    metric5.result = 1;
    metric6.result = 0;

    line_sender sender;

    sender.add_metric(&metric1);
    sender.add_metric(&metric2);
    sender.add_metric(&metric3);
    sender.add_metric(&metric4);
    sender.add_metric(&metric5);

    ASSERT_EQ( 3, sender.calculate_star_rating() );
}

TEST(line_sender, 1_failing_metrics_and_5_passing_metrics_return_4_star_rating)
{

    metric_stub fake_metric[6];
    fake_metric[5].result = 0;

    line_sender sender;

    for(metric_stub* p= fake_metric; p != &fake_metric[6]; ++p)
    {
        sender.add_metric(p);
    }

    ASSERT_EQ( 4, sender.calculate_star_rating() );

}
