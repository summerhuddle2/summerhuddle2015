#include "line_sender.h"
#include <iostream>

void line_sender::add_metric(metric *target_metric)
{
    stored_metric.push_back (target_metric);
}


void line_sender::extract_lines(std::istream & stream)
{
    std::string line;

    while (stream.eof() == false)
    {
        getline(stream, line);
        for (size_t i=0;i<stored_metric.size();i++)
        {
            stored_metric[i]->parse_line(line);
        }
    }
}

int line_sender::calculate_star_rating()
{
    float star_rating=0;

    for (size_t i=0;i<stored_metric.size();i++)
        {
            star_rating+=stored_metric[i]->metric_result();
        }

    if (star_rating==0)
    {
        return 0;
    }

    star_rating=(star_rating/stored_metric.size())*5;

    return static_cast<int>(star_rating);
}
