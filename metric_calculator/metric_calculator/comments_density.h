#ifndef COMMENTS_DENSITY_H_INCLUDED
#define COMMENTS_DENSITY_H_INCLUDED

#include "metric.h"


class comments_density : public metric
{
public:
    comments_density();
    virtual void parse_line(const std::string & line);
    int metric_result();

private:
    std::size_t cumulative_total;
    std::size_t line_length;
    std::size_t comment_chars;
    int percent_comments;

};

#endif

