#include "depth.h"
#include <string>
#include <vector>

const int threshold=6;

using namespace std;

depth::depth() : depths(1, 0)
{
    depth_count=0;
    number_of_functions=0;
}

void depth::parse_line(const std::string &line )
{
    for(size_t i=0; i<line.length();i++)
    {
        if (line[i]=='{')
            depth_count++;
	    if(depths[number_of_functions]<depth_count)
		depths[number_of_functions]=depth_count;

        if (line[i]=='}')
        {
            depth_count--;

            if (depth_count==0)
            {
                number_of_functions++;
                depths.push_back (0);
            }
        }
    }
}

int depth::metric_result()
{
	int max=0;
	for(size_t i=0; i<depths.size();i++)
	{
		if(max<depths[i])
		max=depths[i];
	}
	if (max<threshold)
        return 1;

    return 0;
}
