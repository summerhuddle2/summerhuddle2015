#include "cyclomatic_complexity.h"
#include "counter.h"
#include <string>

using namespace std;

cyclomatic_complexity::cyclomatic_complexity() : lines_number(0), if_number(0), for_number(0)
{
}

void cyclomatic_complexity::parse_line(const string & line)
{
    if_number += counter(line, "if");
    for_number += counter(line, "for");

    ++lines_number;
}

int cyclomatic_complexity::metric_result()
{
    if (if_and_for_density_score())
        return 0;   // High complexity translates to a bad score
    return 1;       // Low complexity translates to a good score
}

int cyclomatic_complexity::if_count() const
{
    return if_number;
}

int cyclomatic_complexity::for_count() const
{
    return for_number;
}

bool cyclomatic_complexity::if_score() const
{
    if (if_number >= 25)
        return true;
    else
        return false;
}

bool cyclomatic_complexity::if_and_for_density_score() const
{
    if (lines_number == 0)
        return false;

    int if_and_for_density = (100 * (for_number + if_number)) / lines_number;
    if(if_and_for_density >= 50)
        return true;
    else
        return false;
}


