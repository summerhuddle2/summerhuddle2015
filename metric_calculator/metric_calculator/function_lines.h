#ifndef FUNCTION_LINES_INCLUDED
#define FUNCTION_LINES_INCLUDED

#include "metric.h"
#include <vector>

class function_lines : public metric
{
public:
    function_lines(); //This is the constructor

    virtual void parse_line(const std::string & line);
    float average_lines_per_function();
    int metric_result();
private:
    int number_of_lines;
    int depth_count;
    int number_of_functions;
    std::vector <int> line_count;

};


#endif
