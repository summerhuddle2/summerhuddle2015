#ifndef FILE_EXISTS_H_INCLUDED
#define FILE_EXISTS_H_INCLUDED

#include <fstream>

class parameters {
public:
    parameters (int argc, char** argv);
    bool exists();
    std::istream & input_stream(){
        return myfile;
    }
private:
    std::ifstream myfile;
};




#endif
