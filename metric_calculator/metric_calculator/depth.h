#ifndef DEPTH_INCLUDED
#define DEPTH_INCLUDED

#include "metric.h"
#include <vector>

class depth : public metric
{
public:
    depth(); //This is the constructor

    virtual void parse_line(const std::string & line);
    int metric_result();
private:
    int depth_count;
    int number_of_functions;
    std::vector <int> depths;

};


#endif
