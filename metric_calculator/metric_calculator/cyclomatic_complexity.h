#ifndef CYCLOMATIC_COMPLEXITY_H_INCLUDED
#define CYCLOMATIC_COMPLEXITY_H_INCLUDED

#include "metric.h"

class cyclomatic_complexity : public metric
{
public:
    cyclomatic_complexity();
    virtual void parse_line(const std::string & line);
    int metric_result();

    int if_count() const;
    int for_count() const;
    bool if_score() const;
    bool if_and_for_density_score() const;

private:
    int lines_number;
    int if_number;
    int for_number;
};


#endif
